﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WPFDataGridBackground
{
    class CellData
    {
        public string Data1 { get; set; }
        public string Data2 { get; set; }
        public string Data3 { get; set; }
    }
    /// <summary>
    /// MainWindow.xaml の相互作用ロジック
    /// </summary>
    public partial class MainWindow : Window
    {
        /**
         * @brief コンストラクタ
         */
        public MainWindow()
        {
            InitializeComponent();

            // データグリッドにサンプルデータを設定します。
            var items = new List<CellData>();
            items.Add(new CellData() { Data1 = "Data11", Data2 = "", Data3 = "" });
            items.Add(new CellData() { Data1 = "", Data2 = "Data22", Data3 = "" });
            items.Add(new CellData() { Data1 = "", Data2 = "", Data3 = "Data33" });
            dataGrid.ItemsSource = items;
        }

        /**
         * ウィンドウが描画された後に呼び出されます。
         * 
         * @param [in] sender ウィンドウ
         * @param [in] e イベント
         */
        private void Window_ContentRendered(object sender, EventArgs e)
        {
            // データグリッドのセル数分繰り返します。
            var rowNum = dataGrid.Items.Count;
            var columnNum = dataGrid.Columns.Count;
            for (int i = 0; i < rowNum; ++i)
            {
                for (int j = 0; j < columnNum; ++j)
                {
                    // セルにデータが設定されている場合、背景色をグリーンに変更します。
                    var cell = GetDataGridCell(dataGrid, i, j);
                    var content = cell.Content;
                    var textBlock = content as TextBlock;
                    var text = textBlock.Text;
                    if (string.IsNullOrEmpty(text)) continue;
                    textBlock.Background = Brushes.Green;
                }
            }
        }

        // 内部メソッド(詳細は省略)

        public DataGridCell GetDataGridCell(DataGrid dataGrid, int rowIndex, int columnIndex)
        {
            if (dataGrid.Items == null || dataGrid.Items.IsEmpty)
            {
                return null;
            }

            var row = GetDataGridRow(dataGrid, rowIndex);
            if (row == null)
            {
                return null;
            }

            var presenter = GetVisualChild<DataGridCellsPresenter>(row);
            if (presenter == null)
            {
                return null;
            }

            var generator = presenter.ItemContainerGenerator;
            var cell = generator.ContainerFromIndex(columnIndex) as DataGridCell;
            if (cell == null)
            {
                dataGrid.UpdateLayout();
                var column = dataGrid.Columns[columnIndex];
                dataGrid.ScrollIntoView(row, column);
                cell = generator.ContainerFromIndex(columnIndex) as DataGridCell;
            }
            return cell;
        }

        public DataGridRow GetDataGridRow(DataGrid dataGrid, int index)
        {
            if (dataGrid.Items == null || dataGrid.Items.IsEmpty)
            {
                return null;
            }

            var generator = dataGrid.ItemContainerGenerator;
            var row = generator.ContainerFromIndex(index) as DataGridRow;
            if (row == null)
            {
                dataGrid.UpdateLayout();
                var item = dataGrid.Items[index];
                dataGrid.ScrollIntoView(item);
                row = generator.ContainerFromIndex(index) as DataGridRow;
            }
            return row;
        }

        private T GetVisualChild<T>(Visual parent) where T : Visual
        {
            T result = default(T);
            var count = VisualTreeHelper.GetChildrenCount(parent);
            for (int i = 0; i < count; ++i)
            {
                var child = VisualTreeHelper.GetChild(parent, i) as Visual;
                result = child as T;
                if (result != null)
                {
                    break;
                }

                result = GetVisualChild<T>(child);
            }
            return result;
        }
    }
}
